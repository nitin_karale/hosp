/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.digitals.hosp.controller;

import com.digitals.hosp.models.User;
import com.digitals.hosp.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

/**
 *
 * @author Nitin
 */
@Controller
public class MainController {

    @Autowired
    UserService userService;

    @RequestMapping("/")
    public String home() {
        System.out.println("This is home");
        return "home";
    }

    @RequestMapping("/login")
    public String login() {
        return "login";
    }

    @RequestMapping("/userLogin")
    public String save(Model model, @RequestParam("email") String email,
                    @RequestParam("password") String password) {

        System.out.println("Email Id :=" + email);
        System.out.println("Password:=" + password);

        User user = userService.getUser(email, password);
        
        if (user != null) {
            model.addAttribute("name", "Nitin" + user.getLastName());
            return "dashboard";
        } else {
            return "login";
        }
    }
}
