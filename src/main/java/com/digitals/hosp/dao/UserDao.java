/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.digitals.hosp.dao;

import com.digitals.hosp.models.User;
import java.util.List;

/**
 *
 * @author Nitin
 */
public interface UserDao {

    public User getUser(String email, String password);

    public List<User> getAllUser();

}
