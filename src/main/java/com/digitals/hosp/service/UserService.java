/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.digitals.hosp.service;

import com.digitals.hosp.dao.UserDao;
import com.digitals.hosp.models.User;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author Nitin
 */
@Service
public class UserService {
    
    @Autowired
    UserDao userDao;

    public User getUser(String email, String password) {
        User user = userDao.getUser(email, password);

        System.out.println("user :=" + user);
        return user;
    }
    
    public List<User> getAllUser() {
        return userDao.getAllUser();
    }

}
